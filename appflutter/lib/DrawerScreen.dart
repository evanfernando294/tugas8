import 'package:flutter/material.dart';

class DrawerScreen extends StatefulWidget {
  @override
  _DrawwerScreenState createState() => _DrawwerScreenState();
}

class _DrawwerScreenState extends State<DrawerScreen> {
  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: ListView(
      children: <Widget>[
        UserAccountsDrawerHeader(
          accountName: Text("Evan          "),
          currentAccountPicture:
              CircleAvatar(backgroundImage: AssetImage("assets/img/hilmy.jpg")),
          accountEmail: Text("                .com"),
        ),
        DrawerListTile(
          iconData: Icons.group,
          title: "NewGroup",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.group,
          title: "New Secret Group",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.group,
          title: "New Channel Chat",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.group,
          title: "contacts",
          onTilePressed: () {},
        ),
  DrawerListTile(
          iconData: Icons.group,
          title: "Saved Message",
          onTilePressed: () {},
        ),
        DrawerListTile(
          iconData: Icons.group,
          title: "Calls",
          onTilePressed: () {},
        )
      ],
    ));
  } 
}

  class DrawerListTitle extends StatelessWidget {
    final iconData? iconData;
    final string? title;
    final VoidCallback? onTilePressed;

    const DrawerListTile(
      {Key? key, this.iconData, this.title, this.onTilePressed})
      : super(key: key);
    @override
    Widget build(BuildContext context) {
      return ListTile(
        onTap: onTilePressed,
        dense: true,
        leading: Icon(iconData),
        title: Text(
          title!,
          style: TextStyle(fontSize: 16),
        ),
      );
    }
  }
  